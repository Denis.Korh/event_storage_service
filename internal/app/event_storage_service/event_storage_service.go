package eventstorageservice

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/asaskevich/govalidator"
)

type Event struct {
	Type        string    `json:"type" bson:"type"`
	State       int       `json:"state" bson:"state"`
	Started_at  time.Time `json:"started_at" bson:"started_at"`
	Finished_at time.Time `json:"finished_at" bson:"finished_at"`
}

type Model interface {
	FindEvent(eventType string) (result *Event)
	StartEvent(eventType string) (err error)
	FinishEvent(eventType string) (err error)
}

type event_storage_service struct {
	db Model
}

func NewApplication(db Model) *event_storage_service {
	return &event_storage_service{db: db}
}

func (ess *event_storage_service) startEvent(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var IncomingEvent Event
	_ = json.NewDecoder(r.Body).Decode(&IncomingEvent)
	
	if !govalidator.IsAlphanumeric(IncomingEvent.Type) {
		json.NewEncoder(w).Encode("Event name must contain only numbers and Latin letters")

		return
	}

	foundEvent := ess.db.FindEvent(IncomingEvent.Type)

	if foundEvent == nil {
		err := ess.db.StartEvent(IncomingEvent.Type)
		if err != nil {
			log.Fatal(err)
		}

		json.NewEncoder(w).Encode(fmt.Sprintf("Event '%s' started", IncomingEvent.Type))
	} else {
		json.NewEncoder(w).Encode("Event already exists")
	}
}

func (ess *event_storage_service) finishEvent(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var IncomingEvent Event
	_ = json.NewDecoder(r.Body).Decode(&IncomingEvent)

	if !govalidator.IsAlphanumeric(IncomingEvent.Type) {
		json.NewEncoder(w).Encode("Event name must contain only numbers and Latin letters")

		return
	}

	foundEvent := ess.db.FindEvent(IncomingEvent.Type)

	if foundEvent != nil && foundEvent.State == 0 {
		err := ess.db.FinishEvent(IncomingEvent.Type)
		if err != nil {
			log.Fatal(err)
		}

		json.NewEncoder(w).Encode(fmt.Sprintf("The event '%s' has been completed", IncomingEvent.Type))
	} else {
		json.NewEncoder(w).Encode("Event already done or doesn't exists")
	}
}

func (ess *event_storage_service) StartApp(port string) {
	r := mux.NewRouter()
	r.HandleFunc("/v1/start", ess.startEvent).Methods("POST")
	r.HandleFunc("/v1/finish", ess.finishEvent).Methods("POST")

	fmt.Println("Server started at " + port)
	http.ListenAndServe(port, r)
}
