package model

import (
	"context"
	app "event_storage_service/internal/app/event_storage_service"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type model struct {
	client     *mongo.Client
	collection *mongo.Collection
}

func NewModel(client *mongo.Client, collection *mongo.Collection) *model {
	return &model{client, collection}
}

func (m *model) FindEvent(eventType string) (result *app.Event) {
	filter := bson.D{{"type", eventType}}
	_ = m.collection.FindOne(context.TODO(), filter).Decode(&result)

	return result
}

func (m *model) StartEvent(eventType string) (err error) {
	startedEvent := &app.Event{eventType, 0, time.Now(), time.Time{}}

	_, err = m.collection.InsertOne(context.TODO(), startedEvent)

	return
}

func (m *model) FinishEvent(eventType string) (err error) {
	filter := bson.D{{"type", eventType}}
	update := bson.D{
		{"$set", bson.D{
			{"state", 1},
			{"finished_at", time.Now()},
		}},
	}

	_, err = m.collection.UpdateOne(context.TODO(), filter, update)

	return
}
